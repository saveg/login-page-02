<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page 2</title>

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>
<body>

    <main class="container">
        <section class="wrapper">
            <form action="login.php" method="POST" class="login-form">

                <img src="imgs/login-icon.svg" alt="Login Icon">
                <h1>Welcome Back!</h1>

                    <span>
                        <?php if(isset($error)){ 
                            echo $error; 
                            }
                        ?>
                    </span>

                <label for="mail">E-mail</label>
                <input type="email" id="mail" name="email" required>
                <label for="password">Password</label>
                <input type="password" id="pass" name="password1" required>

                <input type="submit" name="submit" value="Sign in" class="signin-button">
                <a href="signup.php" class="link">Don't have an account</a>
            </form>
        </section>
    </main>
    
</body>
</html>