<?php

    session_start();
    if(!empty($_SESSION['email'])){
        header('Location: first-page.php');
    }else{
        header('Location: index.php');
    }

    require 'connection.php';

    if(isset($_POST['submit'])){

        $email = $_POST['email'];
        $password = $_POST['password1'];

        if(empty($email) || empty($password)){
            $error = 'All fields are required'; 
        }else{
            $query = $conn->prepare("SELECT email, password FROM login WHERE email=? AND password=?");

            $query->execute(array($email, $password));
            $row = $query->fetch(PDO::FETCH_BOTH);

            if($query->rowCount() > 0){
                $_SESSION['email'] = $email;
                header('Location: first-page.php');
            }else{
                $error = "E-mail or Password is Incorrect!";
            }
        }
    }

?>