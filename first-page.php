<?php

    session_start();
    if(isset($_SESSION['email'])){
        $user = $_SESSION['email'];
    }else{
        header('Location: index.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page 2</title>

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>
<body>

    <main class="container">
        <section class="wrapper">
            <div class="login-form">

                <img src="imgs/login-icon.svg" alt="Login Icon">
                <h1>Welcome, <?php $user ?></h1>
                <a href="logout.php">Logout</a>
                
            </div>
        </section>
    </main>
    
</body>
</html>