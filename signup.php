<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page 2</title>

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>
<body>

    <main class="container">
        <section class="wrapper">
            <form action="register.php" method="POST" class="login-form">

                <img src="imgs/login-icon.svg" alt="Login Icon">
                <h1>Welcome!</h1>
                <span></span>
                <label for="mail">E-mail</label>
                <input type="email" id="mail" name="email" placeholder="E-mail..." required>
                <label for="password">Password</label>
                <input type="password" id="pass" name="password1" placeholder="Password..." required>
                <label for="password">Repeat Password</label>
                <input type="password" id="pass" name="password2" placeholder="Repeat password..." required>

                <input type="submit" name="submit" value="Sign up" class="signin-button">
                <a href="index.php" class="link">Already have an account</a>
            </form>
        </section>
    </main>
    
</body>
</html>